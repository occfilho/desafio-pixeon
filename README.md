# Desafio Pixeon

Implemente um função que recebe:
 Dois inteiros, n, m > 0.
 Um array A de tamanho n, contendo n inteiros ordenados nas posições 0
a n-1.
 Um array B de tamanho m, contendo m inteiros ordenados nas posições
0 a m-1.
- Objetivo:
 Armazenar os n elementos de A e os m elementos de B no array C, de
modo que ele fique com os n + m elementos ordenados.
 Não utilize funções de ordenação auxiliares como Collections.sort()
ou Arrays.sort().
 Analisar a complexidade do seu algoritmo.