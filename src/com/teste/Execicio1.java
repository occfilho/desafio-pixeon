package com.teste;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Execicio1 {

    public static void criarArrayDeInteirosOrdenado(Integer n, Integer m) {


        var A = IntStream.range(1, n).toArray();

        var B = IntStream.range(1, m).toArray();

        var C = IntStream.concat(Arrays.stream(A), Arrays.stream(B)).toArray();

        for (var i = 0; i < C.length; i++) {
            for (var j = 0; j < C.length; ++j) {
                if (C[i] < C[j]) {
                    var valor = C[j];
                    C[j] = C[i];
                    C[i] = valor;
                }
            }
        }

        System.out.println(Arrays.toString(C));

    }

}
